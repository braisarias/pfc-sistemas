/* 
 * File:   main.c
 * Author: braisarias
 *
 * Created on 4 de Outubro de 2012, 18:14
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#define MAX_BUFFER 256
#define MAXCHAR 2048
#define MAX_FILENAME 2048
#define SLEEP_TIME 2
#define MAXCOMMAND 2048

#define DEFAULT_CONF_FILE "openBeem.cfg\0"
#define DEFAULT_DEV_FILE "/dev/ttyUSB0\0"
#define DEFAULT_FILE_DUMP "openBeem-RSSI.dump\0"
#define AT_MODE_ENTER "+++\0"
#define AT_MODE_EXIT "ATCN\0"
#define AT_COMMAND_GETRSSI "ATDB\n\0"
#define ECHO_MSG "a\0"
#define CONF_PORT_COMMAND_START "stty -F "
#define CONF_PORT_COMMAND_END " 9600 cs8 -cstopb\0"
//stty -F /dev/ttyUSB0 cs8 9600 ignbrk -brkint -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts
//stty -F /dev/ttyUSB0 cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts
//stty -F /dev/ttyUSB0 9600 -parenb -parodd cs8 -hupcl -cstopb cread clocal -crtscts -iuclc -ixany -imaxbel -iutf8 -opost -olcuc -ocrnl -onlcr -onocr -onlret -ofill  -ofdel nl0 cr0 tab0 bs0 vt0 ff0 -isig -icanon -iexten -echo -echoe -echok -echonl -noflsh -xcase -tostop -echoprt -echoctl -echoke


//CONF_PORT_COMMAND_END " 9600 cs8 -cstopb\0"


typedef struct {
	/* number of iterations (values of RSSI) */
	int it;
	/* filename where put data */
	char filename[MAX_FILENAME];
	/* filename of device is mapped*/
	char dev[MAX_FILENAME];
    /* verbose mode on if verb=1 off if verb=0*/
    char verb;
} t_conf;




/**
 * DEPRECATED   This function print the sintax of this program
 * FIX IT
 */
void printSintax(char err){
    printf("DEPRECATED INFO\n");
	if (err)
		printf("Syntax error\n");
		
	printf("getrssi <dev> [-i n] [-f filename] \nget RSSI from device name dev\n");
	
	printf("getrssi -h\n");
	printf("getrssi --help\tprint help page\n");

	printf("OPTIONS:\n");
	printf("(*) -i n\tGet n values of RSSI\n");
	
	printf("(*) -f filename\tStore the values in file named filename\n");

	printf("\n(*) NOT IMPLEMENTED YET\n");
    printf("\n\n");
}


void confDefault(t_conf *c){
    c->it = 1; //only 1 value of RSSI
    c->verb = 1; //by default, verbose mode is enable
    strcpy(c->dev, DEFAULT_DEV_FILE);
    strcpy(c->filename, DEFAULT_CONF_FILE);
}


int isValidConfLine(char *l){
    if (l[0] == '#'){ //comment line
        return 0;
    }
    if (strcmp(l, "") == 0){ //blank line
        return 0;
    }
    if (strcmp(l, " ") == 0){ //line with only space
        return 0;
    }
    if (strchr(l,'=') == NULL){ //line with no '=' char
        return 0;
    }
    return 1;
}

/* función que colle a configuración do ficheiro */
/* example of configuration file:

#filename to write data
filename=rssi_datos.dat
#iterations
it=2
#device 
device=/dev/ttyUSB0
* 
* */
int getConf(char *conf_file, t_conf *c) {
    FILE *f;
    char linha[MAXCHAR], propiedade[MAXCHAR], valor[MAXCHAR];


    if ((f = fopen(conf_file, "rt")) == NULL) {
        perror("Error opening configuration file\n");
        return EXIT_FAILURE;
    }

    //FIX IT: pode que non funcione ben; que trague algunha merdalla
    
    while (fgets(linha, MAXCHAR, f) != NULL) {
        if (isValidConfLine(linha)) {
            // linha have a '=' char
            sscanf(linha, "%s=%s", propiedade, valor);

            if (strcmp(propiedade, "filename") == 0) {
                strcpy(c->filename, valor);
            } else {
                if (strcmp(propiedade, "it") == 0) {
                    c->it = atoi(valor);
                } else {
                    if (strcmp(propiedade, "device") == 0) {
                        strcpy(c->dev, valor);
                    }
                }
            }
        }
    }
    fclose(f);
    return EXIT_SUCCESS;
}




/**
 * This funciton send a command to device
 * dev 		: the device file descriptor
 * command	: the command/string you'll send
 * devname 	: the filename of the device
 */
int send_command(int dev, char *command, char *devname, char verb){
	ssize_t rtn_write;
	
	if (verb){
		printf("sending '%s' to '%s'\n", command, devname);
	}
	
	if ((rtn_write = write(dev, command, strlen(command))) != strlen(command)){
		/* don't send all character of command */
		
		if (rtn_write == -1){
			/* error write on "file" sending to serial port */
			if (verb){
				printf("Error sending '%s' to '%s'\n", command, devname);
			}
			return -1;
		}
		if (verb){
			printf("sent %d chars of '%s' to '%s'\n", (int)rtn_write, command, devname);
		}
	}
	return EXIT_SUCCESS;
}




/**
 * this function receive a response from device
 * dev 		: the device file descriptor
 * devname 	: the filename of the device
 * buffer	: the buffer where the response will be in
 */
int receive_response(int dev, char *devname, char *buffer, char verb){
	ssize_t rtn_read;
	
	if (verb){
		printf("receiving response to '%s'\n", devname);
	}
	
	if ((rtn_read = read(dev, buffer, MAX_BUFFER-1)) == -1){
		/* error reading serial port */
		if (verb){
			printf("Error receiving response to '%s'\n", devname);
		}
		return -1;
	}
	if (verb){
		printf("received %d chars: '%s'\n", (int)rtn_read, buffer);
	}
	return (EXIT_SUCCESS);
}

/**
 * This function open a device
 * @param usbdev file descriptor of usb device
 * @param verb
 * @param devname
 * @return 
 */
int openDev(int *usbdev, char verb, char *devname){
    if (verb){
		printf("openning '%s'\n", devname);
	}
	if ((*usbdev = open(devname, O_RDWR)) == -1){
		if (verb){
			printf("Error open '%s'\n", devname);
		}
		return EXIT_FAILURE;
	}
    return(EXIT_SUCCESS);
}



int commandModeExit(int usbdev, char verb, char *devname){
    if (send_command(usbdev, AT_MODE_EXIT, devname, verb) != 0){
		return EXIT_FAILURE;
	}
    return EXIT_SUCCESS;
}


/**
 * 
 * @param devname
 */
void confSerialPort(char *devname){
    char *command;
    command = (char *) malloc(MAXCOMMAND * sizeof(char));
    
    /* isto é o que fai na web esa:
    1)I added a system call to my code using stty for 115200 Baud 8N1:
    system("stty -F /dev/ttyUSB0 115200 cs8 -cstopb -parity -icanon min 1 time 1"); */
    
    //build command line
    strcpy(command, CONF_PORT_COMMAND_START);
    strncat(command, devname, strlen(devname)-1);
    strcat(command, CONF_PORT_COMMAND_END);
    //executing command line
    system(command);
    
    free(command);
}


int sendEcho(int usbdev, char* devname, char verb, int *cmp){
    char *msg, *response;
    
    /* send something */
	msg = malloc(sizeof(char) * MAX_BUFFER);
    
	strcpy(msg, ECHO_MSG);
    
	if (send_command(usbdev, msg, devname, verb) != 0){
        free(msg);
		return EXIT_FAILURE;
	}
    
	/* read response */
	response = malloc(sizeof(char) * MAX_BUFFER);
	
	if (receive_response(usbdev, devname, response, verb) != 0){
		free(response);
        free(msg);
		return EXIT_FAILURE;
	}
    
    if (cmp != NULL){ //if need, "return" strcmp value
        *cmp = strcmp(msg, response);
    }
        
	free(response);
    free(msg);
	sleep(SLEEP_TIME);
    
    return EXIT_SUCCESS;
}



int initDumpFile(int *f, t_conf conf){
    //opening file
    if ((*f = open(conf.filename, O_WRONLY | O_CREAT )) == -1){
        perror("Error creating a dump file of RSSI values\n");
        return EXIT_FAILURE;
    }
        
    /*
     * write to file:
     * date with hour
     * description of the values
     * GPS info ?¿¿?
     */
    
    return EXIT_SUCCESS;
}

/**
 * get 1 value of RSSI; the device must be on AT mode
 * @param conf
 * @param usbdev
 * @return 
 */
int get_RSSI_value(t_conf conf, int usbdev){
    /* int usbdev or int *usbdev ? */
    char response[MAX_BUFFER];
    
    /* send ATcomand_getrssi command */
    if (send_command(usbdev, AT_COMMAND_GETRSSI, conf.dev, conf.verb) != 0) {
        return EXIT_FAILURE;
    }

    /* wait 1 second */
    if (conf.verb) {
        printf("wait for response...\n");
    }
    sleep(SLEEP_TIME);

    /* read response */
    if (receive_response(usbdev, conf.dev, response, conf.verb) != 0) {
        return EXIT_FAILURE;
    }
    
    // we need check if response is a correct value of RSSI or be other thing
    return atoi(response);
}


/**
 * This function get the RSSI from devname device
 */
int get_rssi(t_conf conf){
	int usbdev, i, fDump, rssi_value;
	char haveFDump=0;
	char *msg;
	
	if (conf.verb){
		printf("getting %d RSSI values from device '%s'\n", conf.it, conf.dev);
	}
	
    //configuring serial port:
    confSerialPort(conf.dev);
    
	/* open "file" */
    /* REMEMBER CLOSE FILE */
	if (openDev(&usbdev, conf.verb, conf.dev) == EXIT_FAILURE){
        printf("Error openning device\n");
        return EXIT_FAILURE;
    }

	/* send something */
    sendEcho(usbdev, conf.dev, conf.verb, NULL); // don't need strcmp value

    if (initDumpFile(&fDump, conf) == EXIT_SUCCESS){
        haveFDump = 1;
    }
    
	/* send ATmode_enter command */
	if (send_command(usbdev, AT_MODE_ENTER, conf.dev, conf.verb) != 0){
		/* close "file" */
		close(usbdev);
		return EXIT_FAILURE;
	}
	/* wait 1 second */
	if (conf.verb){ printf("entering command mode...\n"); }
	sleep(SLEEP_TIME);
    
    msg = (char *) malloc(sizeof(char*)*MAX_BUFFER);
    receive_response(usbdev, conf.dev, msg, conf.verb);
    free(msg);
    

    for(i=0; i<conf.it; i++){
        if ((rssi_value = get_RSSI_value(conf, usbdev)) == EXIT_FAILURE){
            //not a RSSI value
            //dont write to file
            continue;
        }

        if (conf.verb){
            /* print RSSI ? */
            printf("RSSI value: %d\n", rssi_value);
        }
        
        if(haveFDump){
            printf("write value to file\n");
        }
        
    }
	
    /* exit command mode */
    commandModeExit(usbdev, conf.verb, conf.dev);
	/* close files  */
    if (haveFDump){
        close(fDump);
    }
	close(usbdev);
	return EXIT_SUCCESS;
}






/*
 * 
 */
int main(int argc, char** argv) {
    t_conf configuration;
    
    

    if (getConf(DEFAULT_CONF_FILE, &configuration) == EXIT_FAILURE){
        //default configuration values
        printf("Default configuration values!\n");
        confDefault(&configuration);
    }
    printSintax(0);

    // only implemented get_rssi
    if (get_rssi(configuration) != EXIT_SUCCESS) {
        perror("Error getting RSSI");
    }
    
    return (EXIT_SUCCESS);
}
