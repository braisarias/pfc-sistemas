%{
	#include <stdio.h>
	#include <string.h>

	#define F_OUT "out_preprocesador"

	FILE *f;
%}

newline "\n"
number [0-9]+

RSSI_VALUE	^{number}" "


%%


{RSSI_VALUE}	{
	fprintf(f, "%s\n", yytext);
}

{newline}	{
}
.	{
}

%%

int main(){
	f = fopen(F_OUT, "w");
	yylex();
	fclose(f);
	
}

