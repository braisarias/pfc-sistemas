% graficas dos experimentos

%% Clear all
close all;
clear all;
clc;

%% Read data from files
%dataStr = cell(10, 2);

dataStr(1,1) = {'exterior 5-6 m'};
dataStr(1,2) = {'ext_5-6m_.data'};
dataStr(2,1) = {'exterior 60 m'};
dataStr(2,2) = {'ext_60m_.data'};
dataStr(3,1) = {'exterior 80 m'};
dataStr(3,2) = {'ext_80m_.data'};

dataStr(4,1) = {'interior 5-6 m'};
dataStr(4,2) = {'int_5-6m_.data'};
dataStr(5,1) = {'interior 20 m'};
dataStr(5,2) = {'int_20m_.data'};
dataStr(6,1) = {'interior 40-50 m'};
dataStr(6,2) = {'int_40-50m_.data'};

dataStr(7,1) = {'urbán 20 m'};
dataStr(7,2) = {'urban_020m_.data'};
dataStr(8,1) = {'urbán 70 m'};
dataStr(8,2) = {'urban_070m_.data'};
dataStr(9,1) = {'urbán 180 m'};
dataStr(9,2) = {'urban_180m_.data'};
dataStr(10,1) = {'urbán 400 m'};
dataStr(10,2) = {'urban_400m_.data'};

dataStr(11,1) = {'exterior 100mw 7m'};
dataStr(11,2) = {'ext_100mw_007m.data'};
dataStr(12,1) = {'exterior 100mw 70m'};
dataStr(12,2) = {'ext_100mw_070m.data'};
dataStr(13,1) = {'exterior 100mw 100m'};
dataStr(13,2) = {'ext_100mw_100m.data'};
dataStr(14,1) = {'exterior 100mw 120m'};
dataStr(14,2) = {'ext_100mw_120m.data'};

nExperimentos = size(dataStr, 1);
distancias = zeros(nExperimentos, 1);
distancias(1) = 5.5;
distancias(2) = 60;
distancias(3) = 80;
distancias(4) = 5.5;
distancias(5) = 20;
distancias(6) = 45;
distancias(7) = 20;
distancias(8) = 70;
distancias(9) = 180;
distancias(10) = 400;
distancias(11) = 7;
distancias(12) = 70;
distancias(13) = 100;
distancias(14) = 120;

% 720 is max size of RSSI values file
data = zeros(nExperimentos, 720);
dataSize = zeros(nExperimentos, 1);

% obtemos os datos dos ficheiros
for i = 1:nExperimentos;
    d = textread(dataStr{i,2},'%d%*[^\n]');
    dataSize(i) = size(d, 1);
    data(i, 1:dataSize(i)) = d;
end;

%%
%medias de RSSI:
medias = zeros(nExperimentos, 1);
for i=1:nExperimentos;
    medias(i) = mean(data(i,1:dataSize(i)));
end;

%% Calculo das ns
% n = (-rssi-A)/(log_10(d)*10)
A = 64;

%ns candidatos
ns_candidatas = (-medias-A)./(log10(distancias)*10);

%exterior 300mW (1:3)
%interior 300mW (4:6)
%urban 300mW (7:10)
%exterior 100mW (11:14)
n_exterior300 = mean(ns_candidatas(1:3));
n_interior = mean(ns_candidatas(4:6));
n_urban = mean(ns_candidatas(7:10));
n_exterior100 = mean(ns_candidatas(11:14));

ns=zeros(size(ns_candidatas));
ns(1) = n_exterior300;
ns(2) = n_exterior300;
ns(3) = n_exterior300;
ns(4) = n_interior;
ns(5) = n_interior;
ns(6) = n_interior;
ns(7) = n_urban;
ns(8) = n_urban;
ns(9) = n_urban;
ns(10) = n_urban;
ns(11) = n_exterior100;
ns(12) = n_exterior100;
ns(13) = n_exterior100;
ns(14) = n_exterior100;

%% Calculo de distancias estimadas
% d = 10^((-rssi-A)/(10*n))
%d_estimadas = 10^((-medias-A)./(10*ns))
d_estimadas_medias = zeros(size(medias));
for i=1:size(medias,1);
    d_estimadas_medias(i) = 10^((-medias(i)-A)/(10*ns(i)));
end;

%% Calculo de todas as distancias estimadas 

d_estimadas = zeros(size(data));
for i=1:nExperimentos;
    for j=1:dataSize(i);
        d_estimadas(i,j) = 10^((-data(i,j))./(10*ns(i)));
    end;
    %d_estimadas(i,1:dataSize(i)) = 10^((-data(i,1:dataSize(i)))./(10*ns(i)));
end;

%% Calculo dos erros por experimento

erroAbs = zeros(size(data));
erroRel = zeros(size(data));;
mediaErroRel = zeros(nExperimentos);
mediaErroAbs = zeros(nExperimentos);
for i=1:nExperimentos;
    erroAbs(i,1:dataSize(i)) = abs(d_estimadas(i, 1:dataSize(i)) - distancias(i));
    erroRel(i,1:dataSize(i)) = (erroAbs(i,1:dataSize(i))./distancias(i))*100;
    mediaErroRel(i) = mean(erroRel(i,1:dataSize(i)));
    mediaErroAbs(i) = mean(erroAbs(i,1:dataSize(i)));
%     figure(i);
%         plot(erroAbs(i,1:dataSize(i)));
%         title(['Erro absoluto en' dataStr(i, 1)]);
%         xlabel('Radians');
%         ylabel('Erro');
end;

figure;
    subplot(2,1,1);
        plot(mediaErroAbs);
         ylabel('Erro (m)');
         xlabel('nº experimento');
        title('Media dos erros absolutos');
    subplot(2,1,2);
        plot(mediaErroRel);
        ylabel('Erro (%)');
        xlabel('nº experimento');
        title('Media dos erros relativos');
    

%%
%tenho que facer algo así para todos os datos
%10^((dataStr(12,2)-64)/10*(-65))

% formula do RSSI:
% n : constante de propagación
% A : RSSI a 1m = 64
% rssi = -(10*n*log_10(d) + A)
% n = (-rssi-A)/(log_10(d)*10)
% d = 10^((-rssi-A)/(10*n))



%% Histograms
for i = 1:nExperimentos;
    figure(i);
        hist(data(i,1:dataSize(i)));
        title(dataStr(i, 1));
end;

%% Plots
for i = 1:nExperimentos;
    figure(i);
        plot(data(i,1:dataSize(i)));
        title(dataStr(i, 1));
end;

%% Fitting distributions

for i = 1:nExperimentos;
    dfittool(data(i,1:dataSize(i)), zeros(0), zeros(0), dataStr{i, 1});
end;
