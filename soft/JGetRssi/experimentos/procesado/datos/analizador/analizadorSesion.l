%{
	#include <stdio.h>
	#include <string.h>

	#define F_OUT "out_analizador"

	FILE *f;
	int countEchoFails, countEchoDones;
//Value:	64 2012-12-30 12:36:00:919
%}

digit 			[0-9]
number			{digit}+
year			{number}{4}
month_day		{number}{2}
date			{year}"-"{month_day}"-"{month_day}
hour			{number}":"{number}":"{number}":"{number}
separator		[ \t]
new_line		"\n"
anything		.
complete_date	{date}{separator}{hour}


ECHO_DONE		"echo OK"
ECHO_FAILS		"echo FAIL"
VALUE_LITERAL	"Value:"{separator}
RSSI_VALUE		{number}{separator}{complete_date}
ITERATION		"it "{number}


%s echo_fails
%s value_ignore
%s echo_ok
%s value_write
%s next_step


%%

<INITIAL>{ECHO_DONE} {
	BEGIN (echo_ok);
}

<INITIAL>{ECHO_FAILS} {
	BEGIN (echo_fails);
}

<next_step>{ECHO_DONE} {
	BEGIN (echo_ok);
}

<next_step>{ECHO_FAILS} {
	BEGIN (echo_fails);
}

<echo_ok>{VALUE_LITERAL} {
	BEGIN (value_write);
}

<echo_fails>{VALUE_LITERAL} {
	BEGIN (value_ignore);
}
<value_write>{RSSI_VALUE} {
	// write the value to file
	fprintf(f, "%s\n", yytext);
	//and count the echo ok
	countEchoDones++;
	BEGIN (next_step);
}
<value_ignore>{RSSI_VALUE} {
	// count echo fails
	countEchoFails++;
	BEGIN (next_step);
}
{ITERATION} {
	// after new iteration: next_step
	BEGIN (next_step);
}
{new_line} {
}
{anything} {
}

%%

int main(){
	countEchoDones = countEchoFails = 0;
	f = fopen(F_OUT, "w");
	yylex();
	fclose(f);
	printf("Echo Done: %d\nEcho Fail: %d\nTotal: %d\n", countEchoDones, countEchoFails, countEchoDones+countEchoFails);
}

