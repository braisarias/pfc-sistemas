import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class RSSIValue {
	private int value;
	private Date dateValue;
	private static final String DATE_FORMAT_PATTERN = new String("yyyy-MM-dd HH:mm:ss:SSS");
	
	
	public int getValue() {
		return value;
	}
	
	public Date getDateValue() {
		return dateValue;
	}
	
	public RSSIValue(int value) {
		super();
		this.value = value;
		this.dateValue = new Date();
	}

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		return this.value + " " + df.format(this.dateValue);
	}
	
	
}
