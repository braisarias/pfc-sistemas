
class Configuracion {
	/* number of iterations (values of RSSI) */
	int it;
	/* filename where put data */
	String filename;
	/* filename of device is mapped*/
	String dev;
    /* verbose mode on if verb=1 off if verb=0*/
	Boolean verb;
	/* baudRate */
	int baudRate;
	
	
	
//	public Configuracion(int it, String filename, String dev, Boolean verb) {
//		super();
//		this.it = it;
//		this.filename = filename;
//		this.dev = dev;
//		this.verb = verb;
//		this.baudRate = 9600;
//	}
	
	public Configuracion(int it, String filename) {
		super();
		this.it = it;
		this.filename = filename;
		this.dev = getPortNameByOS();
		this.verb = true;
		this.baudRate = 9600;
	}
	
	
	public Configuracion(){
		super();
		// if iteration time = 5 s, can make 360 iterations for 30 minutes experiment 
		// 300 iterations with time iteration = 5s, experiment 25 minutes
		// experiment 10 minutes with time iteration = 5s, 120 iterations
		this.it = 120;
		// TODO change it by openBeem-RSSI_n.dump if exists openBeem-RSSI.dump
		this.filename = new String("openBeem-RSSI.dump");
		this.dev = getPortNameByOS();
		this.verb = true;
		this.baudRate = 9600;
	}
	
//	public Configuracion(String confFile){
//		// TODO
//		super();
//		// read the configuration file for 
//	}
	



	private String getPortNameByOS() {
		String osname = System.getProperty("os.name","").toLowerCase();

		if ( osname.startsWith("windows") ) {
			return "COM3";
		} else {
			if (osname.startsWith("linux")) {

				return "/dev/ttyUSB0";
			}
			else {
				System.out.println("Unsuported OS");
				return "";
			}

		}
	}

}
