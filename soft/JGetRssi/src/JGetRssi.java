import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class JGetRssi {
	
	
	private static int ITERATION_DELAY = 100;
	private static String ECHO_MSG = new String("a");
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Configuracion conf;
		XbeeDevice device = new XbeeDevice();
		
		System.out.println("JGetRssi");
		
		// arguments parsing
		if (args.length == 2){
			//java JGetRssi 100 file_dump1.dump
			// java JGetRssi filedump.dump
			int it;
			String filename;
			
			try{
				it = Integer.parseInt(args[0]);
				filename = new String(args[1]);
			} catch (NumberFormatException e) {
				try {
					it = Integer.parseInt(args[1]);
				} finally {
					System.err.println("One Argument" + " must be an integer");
			        System.exit(1);
				}
				filename = new String(args[0]);
			}
			conf = new Configuracion(it, filename);
		} else {
			conf = new Configuracion();
		}
		device.initialize();
		getRSSI(device, conf);

	}

	/**
	 * get RSSI values and dump it in a file
	 * @param conf
	 */
	private static void getRSSI(XbeeDevice device, Configuracion conf) {
		ArrayList<RSSIValue> listValues = new ArrayList<RSSIValue>();
		
		
		if (conf.verb){
			System.out.println("getRSSI " + conf.it + " values");
		}
		
		
		for (int i = 0; i < conf.it; i++) {
			try {
				Thread.sleep(ITERATION_DELAY);
			} catch (InterruptedException e2) {
				System.out.println("can not delay");
			}
			
			if (conf.verb){
				System.out.println("it " + i);
			}
			// Send something (echo)
			try {
				if (device.sendEcho(conf, ECHO_MSG) == 0){
					System.out.println("echo OK");
				} else {
					System.out.println("echo FAIL");
					continue;
				}
			} catch (InterruptedException e1) {
				// if echo fails then next iteration
				continue;
			}
			
			// Enter AT mode
			try {
				if (!device.enterCommandMode(conf)){
					System.out.println("unable enter in command mode");
					continue;
				}
			} catch (InterruptedException e1) {
				continue;
			}
			
			
			// get RSSI value and append to list
			try {
				listValues.add(device.getRSSIValue(conf));
			} catch (InterruptedException e) {
				System.out.println("unable get RSSI value");
			}
			
			// exit AT command mode
			try {
				device.exitCommandMode(conf);
			} catch (InterruptedException e) {
				System.out.println("unable exiting AT command mode");
			}
		}
		

		if (listValues.isEmpty()){
			System.out.println("unable to dump values to file : list of value is empty");
		}
		try {
			dump2File(listValues, conf);
		} catch (IOException e) {
			System.out.println("unable to dump values to file");
		}
		
		
		device.close();
	}

	/**
	 * Write to file the RSSI values.
	 * @param listValues
	 * @param conf
	 * @throws IOException
	 */
	private static void dump2File(ArrayList<RSSIValue> listValues, Configuracion conf) throws IOException {
		FileWriter fStream = new FileWriter(conf.filename);
		BufferedWriter out = new BufferedWriter(fStream);
		//TODO write file header
		
		for (RSSIValue rssiValue : listValues) {
			out.write(rssiValue.toString() + "\n");
		}
		out.close();
	}

}
