import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.awt.font.NumericShaper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Enumeration;


/**
 * 
 * @author arduino.cc and braisarias
 * 
 * This Class is a testing class for Serial communication
 *
 */

public class XbeeDevice implements SerialPortEventListener{

	SerialPort serialPort;
	/** The port we're normally going to use. */
	private static final String PORT_NAMES[] = {
		"/dev/tty.usbserial-A9007UX1", // Mac OS X
		"/dev/ttyUSB0", // Linux
		"COM3", // Windows
	};
	/** Buffered input stream from the port */
	private InputStream input;
	/** The output stream to the port */
	private OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;
	
	/** Default milliseconds to wait for echo response. */
	private static final int ECHO_DELAY = 200;
	
	/** 1 second before and after enter command mode */
	private static final int GUARD_TIMES = 1000;
	
	/** */
	private static final int RSSI_DELAY = 500;
	
	
	/* MODES FOR SERIAL EVENT */
	/** unknown mode. */
	private static final int UNKNOWN_MODE = 0;
	/** send and receive mode. */
	private static final int SEND_RECV_MODE = 1;
	/** send and receive mode start */
	private static final int SEND_RECV_MODE_START = 2;
	/** get RSSI mode start */
	private static final int RSSI_MODE_START = 3;
	/** get RSSI mode */
	private static final int RSSI_MODE = 4;
	/** enter command mode start mode */
	private static final int ENTER_CMD_MODE_START = 5;
	/** enter command mode mode */
	private static final int ENTER_CMD_MODE = 6;
	
	
	/** String for put the received message. */
	private StringBuilder recvString;
	/** RSSI value */
	private int rssiValue;
	

	/** String for enter in command mode */
	private static final String AT_MODE_ENTER = new String("+++");
	/** String for exit in command mode */
	private static final String AT_MODE_EXIT = new String ("ATCN\r");
	/** String for get one RSSI value */
	private static final String AT_COMMAND_GETRSSI = new String("ATDB\r");
	
	
	
	/** mode of XBee device:
	 * ECHO_MODE 	: puts the received message into echoString
	 * UNKNOWN_MODE : print the message receive
	 * 2 : */
	private int mode, oldMode;
	

	
	public void initialize() {
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		this.mode = this.oldMode = UNKNOWN_MODE;
		// iterate through, looking for the port
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			for (String portName : PORT_NAMES) {
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			}
		}

		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = serialPort.getInputStream();
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port.
	 * This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	
	private boolean isNumeric(String str){
	  NumberFormat formatter = NumberFormat.getInstance();
	  ParsePosition pos = new ParsePosition(0);
	  formatter.parse(str, pos);
	  return str.length() == pos.getIndex();
	}
	
	/**
	 * parse str, only append hexadecimal numbers
	 * @param str
	 * @return
	 */
	private StringBuilder numericParse(StringBuilder str){
		StringBuilder parsed = new StringBuilder();
		int i;
		char c;
		
		//System.out.println("before '" + str + "'");
		
		for (i=0; i<str.length(); i++){
			c = str.charAt(i);
			//System.out.println("\t\t*switch '" + c + "'");
			switch(c){
				case ' ':
				case '\r':
				case '\t':
				case '\n':
					break;

				default:
					//System.out.println("\t\t*append'" + c + "'");
					parsed.append(c);
					break;
			}
		}
		
		//System.out.println("after '" + parsed + "'");
		return parsed;
	}
	
	
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	private StringBuilder okParse(StringBuilder str) {
		StringBuilder parsed = new StringBuilder();
		int i;
		char c;
	
		for (i=0; i<str.length(); i++){
			c = str.charAt(i);
			switch(c){
				case 'O':
				case 'o':
				case 'K':
				case 'k':
					parsed.append(c);
					break;
			}
		}
		
		return parsed;
	}

	/**
	 * Handle an event on the serial port. Read the data and do action for the mode.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		StringBuilder parsed;
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				
				int available = input.available();
				byte chunk[] = new byte[available];
				input.read(chunk, 0, available);
				StringBuilder received = new StringBuilder( new String(chunk));

				// Displayed results are codepage dependent
				//System.out.println("*EVENT rec: '" + received + "'");
				
				switch (this.mode) {
					case SEND_RECV_MODE_START:
						recvString = new StringBuilder();
						this.mode = SEND_RECV_MODE;
					case SEND_RECV_MODE:
						// append receive to echo String;
						System.out.println("*EVENT SEND_RECV_MODE: '" + received + "'");
						recvString.append(received);
						break;
						
					case RSSI_MODE_START:
						recvString = new StringBuilder();
						this.mode = RSSI_MODE;
					case RSSI_MODE:
						System.out.println("*EVENT RSSI_MODE: '" + received + "'");
						recvString.append(numericParse(received));
						try{
							rssiValue = Integer.parseInt(recvString.toString(),16);
						} catch (NumberFormatException e) {
							rssiValue = 0;
						}
/*						if (isNumeric(parsed.toString())){
							//System.out.print("numeric: '" + parsed + "'");
							//append
							recvString.append(parsed);
						} else{
							//print
							//System.out.print("NON numeric: '" + parsed + "'");
						}*/
						break;

					case ENTER_CMD_MODE_START:
						recvString = new StringBuilder();
						this.mode = ENTER_CMD_MODE;
					case ENTER_CMD_MODE:
						System.out.println("*EVENT ENTER_CMD_MODE: '" + received + "'");
						//parsed = okParse(received);
						recvString.append(received);
						break;
					default:
						System.out.print("*EVENT UNKNOW: '" + received + "'");
						break;
				}
			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
		//Ignore all the other eventTypes, but you should consider the other ones.
	}

	/**
	 * 
	 * @param data data must be writing to serial port
	 * 
	 */
	public void serialWrite(String data){
		byte dataByte[] = new byte[data.length()];
		dataByte = data.getBytes();
		
		try {
			output.write(dataByte);
			System.out.println("\n*write: " + new String(dataByte) + "\n");
		} catch (IOException e) {

			System.err.println(e.toString());
		}
	}
	
/**
 * 
 * @param conf
 * @return difference between send and received strings
 * @throws InterruptedException
 */
	public int sendEcho(Configuracion conf, String msg) throws InterruptedException{
		int cmp;
		if (conf.verb){
			System.out.println("sending echo message");
		}
		
		setMode(SEND_RECV_MODE_START);
		serialWrite(msg);
		
		Thread.sleep(ECHO_DELAY);
		if (recvString == null){
			return -1;
		}
		String recv = new String(recvString);
		setOldMode();
		cmp = msg.compareTo(recv);
		
		if (conf.verb){
			System.out.println("Send:\t" + msg);
			System.out.println("Received:\t" + recv);
			System.out.println("Diff:\t" + cmp);
		}
		
		
		return cmp;
	}
	
	public void setOldMode(){
		this.mode = this.oldMode;
	}
	public void setMode(int newMode){
		this.oldMode = this.mode;
		this.mode = newMode;
	}

	/**
	 * enter in AT command mode
	 * @param conf
	 * @return true if enter in AT command mode, false if not enter in AT command mode
	 * @throws InterruptedException
	 */
	public boolean enterCommandMode(Configuracion conf) throws InterruptedException{
		setMode(ENTER_CMD_MODE_START);
		// No characters sent for one second
		Thread.sleep(GUARD_TIMES);
		
		serialWrite(AT_MODE_ENTER);
		
		// No characters sent for one second
		Thread.sleep(GUARD_TIMES+ECHO_DELAY);//guaded time + for check receive
		if (recvString == null){
			return false;
		}
		String rec = new String(recvString);
		setOldMode();
		System.out.println("AT command rec: " + rec);
		return rec.contains("OK") || rec.contains("ok");
	}
	
	/**
	 * Exit AT command mode
	 * @param conf
	 * @throws InterruptedException
	 */
	public void exitCommandMode(Configuracion conf) throws InterruptedException {
		// No characters sent for one second
		Thread.sleep(GUARD_TIMES);

		serialWrite(AT_MODE_EXIT);

		// No characters sent for one second
		Thread.sleep(GUARD_TIMES);
	}

	/**
	 * get one RSSI value
	 * @param conf
	 * @return RSSIValue this is one RSSI value 
	 * @throws InterruptedException
	 */
	public RSSIValue getRSSIValue(Configuracion conf) throws InterruptedException {
		String msg = new String(AT_COMMAND_GETRSSI);
		RSSIValue value = null;
		
		if (conf.verb){
			System.out.println("receiving RSSI value");
		}
		setMode(RSSI_MODE_START);
		serialWrite(msg);
		
		Thread.sleep(RSSI_DELAY);
		// read the received string
		value = new RSSIValue(rssiValue);
//		String recv = new String(recvString);
//		setOldMode();
//		if (conf.verb){
//			System.out.println("Received:\t" + recv);
//		}
//		try{
//			
//			value = new RSSIValue(Integer.parseInt(recv));
//			
//		} catch (NumberFormatException e){
//			System.out.println("is not a number");
//			//e.printStackTrace();
//		}
		
		
		if (conf.verb && value != null){
			System.out.println("Value:\t" + value);
		}
		
		return value;
	}
	
}
